import React, {useEffect, useState} from 'react';
import {Dimensions, StyleSheet, Text, View, Image} from 'react-native';
import {Provider} from 'react-redux';
import {SplashScreen} from 'expo';
import { Asset } from 'expo-asset';
import {store} from './store';
import Navigation from './components/Navigation';

const {height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    height,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

const App = () => {
  const [areResourcesReady, setResourcesReady] = useState(false);

  useEffect(() => {
    SplashScreen.preventAutoHide();

    cacheResourcesAsync()
      .then(() => setResourcesReady(true))
      .catch(error => console.error(`Unexpected error thrown when loading: ${error.stack}`));
  }, []);

  const cacheResourcesAsync = async () => {
    SplashScreen.hide();

    const images = [
      require('./assets/images/logo.png')
    ];

    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });

    return await Promise.all(cacheImages);
  };

  return (
    !areResourcesReady ? (<View style={styles.container}>
      <Image
        style={{resizeMode: 'contain', width: 150, height: 150 }}
        source={require('./assets/images/logo.png')}
        onLoadEnd={() => {
          SplashScreen.hide();
        }}
        fadeDuration={0.5}
      />
    </View>) : (<Provider store={store}>
      <Navigation />
    </Provider>)
  );
};

export default App;
