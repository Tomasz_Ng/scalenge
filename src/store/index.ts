import {applyMiddleware, compose, createStore} from 'redux';
import {reducers, initialState} from '../reducers';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';

const middlewares = [thunk];
const enhancer = composeWithDevTools;
//const enhancer = env.APP_ENV.indexOf('dev') > -1 ? composeWithDevTools : compose;

export const store = createStore(
  reducers(),
  initialState,
  enhancer(
    applyMiddleware(
      ...middlewares
    )
  )
);
