import Home from '../components/screens/Home';

export default [
    {
        path: '/',
        name: 'home',
        exact: true,
        component: Home
    }
];
