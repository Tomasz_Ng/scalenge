export default {
    "app.language.fr": "Français",
    "app.language.en": "Anglais",
    "screen.login.title": "Connexion",
    "screen.register.title": "Inscription",
    "screen.home.title": "Accueil",
    "screen.dashboard.title": "Tableau de bord",
    "screen.account.title": "Mon compte",
    "login.form.email": "E-mail",
    "login.form.password": "Mot de passe",
    "login.form.btn": "Connexion",
    "login.link.register": "Vous n'avez pas de compte ? Inscrivez vous",
    "register.form.username": "Nom d'utilisateur",
    "register.form.email": "E-mail",
    "register.form.password": "Mot de passe",
    "register.form.confirmed_password": "Confirmer mot de passe",
    "register.form.btn": "Inscription",
    "register.link.login": "Vous êtes déjà membre ? Connectez vous"
};
