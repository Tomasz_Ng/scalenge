import messagesEN from './messages.en';
import messagesFR from './messages.fr';

const messages: any = {
  'en': Object.assign(messagesEN),
  'fr': Object.assign(messagesFR)
};

export default messages;
