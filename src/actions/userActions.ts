import {
  FETCH_LOGIN_PENDING,
  FETCH_LOGIN_SUCCESS,
  FETCH_LOGIN_ERROR,
  FETCH_REGISTER_PENDING,
  FETCH_REGISTER_SUCCESS,
  FETCH_REGISTER_ERROR,
  CLEAN_FORM_ERRORS,
} from '../constants/action-types';

// Login
function fetchLoginPending() {
  return {
    type: FETCH_LOGIN_PENDING
  }
}

function fetchLoginSuccess(response: any) {
  return {
    type: FETCH_LOGIN_SUCCESS,
    response: response
  }
}

function fetchLoginError(error: any) {
  return {
    type: FETCH_LOGIN_ERROR,
    error: error
  }
}

// Register
function fetchRegisterPending() {
  return {
    type: FETCH_REGISTER_PENDING
  }
}

function fetchRegisterSuccess(response: any) {
  return {
    type: FETCH_REGISTER_SUCCESS,
    response: response
  }
}

function fetchRegisterError(error: any) {
  return {
    type: FETCH_REGISTER_ERROR,
    error: error
  }
}

export const fetchData = ({dispatch}: any) => {
  return (url: any, body: any, name: any, method: any, navigation: any) => {
    switch (name) {
      case 'login':
        dispatch(fetchLoginPending());
        break;
      case 'register':
        dispatch(fetchRegisterPending());
        break;
      default:
        break;
    }

    fetch(url, {
      headers: {
        'Content-Type': 'application/json'
      },
      method: method,
      body: body ? body : null,
    }).then(response => response.json().then(response => {
      if (response.errors) {
        throw(response.errors);
      }

      switch (name) {
        case 'login':
          dispatch(fetchLoginSuccess(response));
          navigation.navigate('dashboard');
          break;
        case 'register':
          dispatch(fetchRegisterSuccess(response));
          navigation.navigate('dashboard');
          break;
        default:
          break;
      }
    })).catch(error => {
      switch (name) {
        case 'login':
          dispatch(fetchLoginError(error));
          break;
        case 'register':
          dispatch(fetchRegisterError(error));
          break;
        default:
          break;
      }
    });
  };
};

export const cleanFormErrors = ({dispatch}: any) => {
  return (formName: any, fieldName: any) => {
    dispatch({
      type: CLEAN_FORM_ERRORS,
      formName: formName,
      fieldName: fieldName
    });
  };
};
