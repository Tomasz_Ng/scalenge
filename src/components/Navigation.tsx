import React, {useEffect, useState} from 'react';
import {TouchableOpacity, Image, AsyncStorage} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {connect} from 'react-redux';
import DashboardScreen from './screens/authenticated/Dashboard';
import RegisterScreen from './screens/Register';
import LoginScreen from './screens/Login';
import HomeScreen from './screens/Home';

const Stack = createStackNavigator();

const mapStateToProps = (state: any) => {
  return {
    messages: state.root.messages
  };
};

const Navigation = (props: any) => {
  const [user, setUser] = useState('');

  useEffect(() => {
    loadUser();
    console.log('ok')
  }, []);

  const loadUser = async () => {
    try {
      const user = await AsyncStorage.getItem('user');

      if (user !== null) {
        setUser(user);
      }
    } catch (e) {
      console.error('Failed to load user from storage.')
    }
  };

  return (
    <NavigationContainer
      linking={{
        prefixes: [],
        config: {
          home: 'home',
          login: 'login',
          register: 'register',
          dashboard: 'dashboard'
      }}}>
      <Stack.Navigator
        initialRouteName={!user ? 'home' : 'dashboard'}
        screenOptions={{
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff'
        }}>
        <Stack.Screen
          name="home"
          component={HomeScreen}/>
        <Stack.Screen
          name="login"
          component={LoginScreen}/>
        <Stack.Screen
          name="register"
          component={RegisterScreen}/>
        <Stack.Screen
          name="dashboard"
          component={DashboardScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default connect(mapStateToProps, null)(Navigation);
