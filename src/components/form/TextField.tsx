import React, {useState} from 'react';
import {Platform, StyleSheet, Text, TextInput, View} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux';
import {
  cleanFormErrors
} from '../../actions';

const mapDispatchToProps = (dispatch: any) => {
  return {
    cleanFormErrors: cleanFormErrors({dispatch})
  };
};

const styles = StyleSheet.create({
  fieldsetView: {
    maxWidth: 314,
    width: '100%',
    marginBottom: 15
  },
  inputsetView: {
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 5,
    paddingTop: 5,
    paddingLeft: 15,
    paddingBottom: 5,
    paddingRight: 15,
    height: 50,
  },
  input: {
    width: '100%',
    top: 0,
    left: 0,
    zIndex: 1,
    ...Platform.select({
      web: {
        outlineColor: 'transparent'
      }
    })
  },
  error: {
    color: 'red',
    fontSize: 13,
    marginLeft: 2
  }
});

const scaleDown = {
  from: {
    scale: 1,
    translateY: 8,
    translateX: 0
  },
  to: {
    scale: 0.8,
    translateY: 0,
    translateX: -36
  }
};

const scaleUp = {
  from: {
    scale: 0.8,
    translateY: 0,
    translateX: -36
  },
  to: {
    scale: 1,
    translateY: 8,
    translateX: 0
  }
};

const TextField = (props: any) => {
  const [inputFocused, setInputFocused] = useState('');
  const isInputAnimated = inputFocused === props.name || (inputFocused === props.name && props.value) || inputFocused !== props.name && props.value;

  return (
    <View style={styles.fieldsetView}>
      <View style={[styles.inputsetView, {
        backgroundColor: isInputAnimated ? 'lightgrey' : '#F2F2F2',
        borderBottomWidth: props.error && props.error[props.name] ? 2 : 1,
        borderBottomColor: props.error && props.error[props.name] ? 'red' : 'gray'
      }]}>
        <Animatable.Text
          id={props.name}
          animation={isInputAnimated ? scaleDown : scaleUp}
          duration={200}
          useNativeDriver={true}>
            {props.label}
        </Animatable.Text>

        <TextInput
          value={props.value}
          aria-labelledby={props.name}
          secureTextEntry={props.name === 'password' || props.name === 'confirmedPassword'}
          style={[styles.input, {
            position: isInputAnimated ? 'relative' : 'absolute',
            height: isInputAnimated ? 23 : 50
          }]}
          onChangeText={(value) => {
            props.setInputValue(value);
            props.value === '' ? props.cleanFormErrors(props.form, props.name) : null
          }}
          onFocus={() => setInputFocused(props.name)}
          onBlur={() => setInputFocused('')}
        />
      </View>

      {props.error && props.error[props.name] ? <Text style={styles.error}>{props.error[props.name].message}</Text> : null}
    </View>
  );
};

export default connect(null, mapDispatchToProps)(TextField);
