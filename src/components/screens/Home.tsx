import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import { Section } from '@expo/html-elements';

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

const Home = (props: any) => {
  return (
    <Section style={styles.center}>
      <Text>Home</Text>
      <Button
        title="Login"
        onPress={() => props.navigation.navigate('login')}
      />
    </Section>
  );
};

export default Home;
