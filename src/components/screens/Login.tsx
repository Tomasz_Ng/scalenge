import React, {useState, useEffect} from 'react';
import {StyleSheet, ScrollView, TouchableOpacity, Text, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import TextInputField from '../form/TextField';
import {
  fetchData,
  cleanFormErrors
} from '../../actions';

const mapStateToProps = (state: any) => {
  return {
    error: state.user.loginResponse.error,
    messages: state.root.messages,
    apiHost: state.root.apiHost
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchData: fetchData({dispatch}),
    cleanFormErrors: cleanFormErrors({dispatch})
  };
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1
  },
  scrollView: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15
  },
  button: {
    marginBottom: 15,
    backgroundColor: '#2196f3',
    borderRadius: 5,
    padding: 10,
    width: 314,
    alignItems: 'center'
  },
  buttonText: {
    color: 'white',
    fontWeight: '500'
  },
  link: {
    textAlign: 'center',
    width: 314,
  }
});

const Login = (props: any) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    return props.navigation.addListener('state', () => {
      setEmail('');
      setPassword('');
      props.cleanFormErrors('login', null);
    });
  }, [props.navigation]);

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <TextInputField
          name="email"
          label={props.messages['login.form.email']}
          error={props.error ? props.error : null}
          form='login'
          value={email}
          setInputValue={setEmail}/>

        <TextInputField
          name="password"
          label={props.messages['login.form.password']}
          error={props.error ? props.error : null}
          form='login'
          value={password}
          setInputValue={setPassword}/>

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            props.fetchData(props.apiHost + '/login', JSON.stringify({
              email: email,
              password: password
            }), 'login', 'POST', props.navigation);
          }}>
            <Text style={styles.buttonText}>{props.messages['login.form.btn'].toUpperCase()}</Text>
        </TouchableOpacity>

        <Text
          style={styles.link}
          onPress={() => props.navigation.navigate('register')}>
            {props.messages['login.link.register']}
        </Text>
      </ScrollView>
    </SafeAreaView>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
