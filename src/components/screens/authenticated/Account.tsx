import React, {useEffect, useState} from 'react';
import {StyleSheet, TextInput, View, Button, Alert, Text} from 'react-native';

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
  }
});

const Account = (props: any) => {
  return (
    <View style={styles.center}>
      <Text>Profile</Text>
    </View>
  );
};

export default Account;
