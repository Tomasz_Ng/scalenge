import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, Text, ScrollView, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import TextInputField from '../form/TextField';
import {
  fetchData,
  cleanFormErrors
} from '../../actions';

const mapStateToProps = (state: any) => {
  return {
    error: state.user.registerResponse.error,
    messages: state.root.messages,
    apiHost: state.root.apiHost
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchData: fetchData({dispatch}),
    cleanFormErrors: cleanFormErrors({dispatch})
  };
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1
  },
  scrollView: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15
  },
  button: {
    marginBottom: 15,
    backgroundColor: '#2196f3',
    borderRadius: 5,
    padding: 10,
    width: 314,
    alignItems: 'center'
  },
  buttonText: {
    color: 'white',
    fontWeight: '500'
  },
  link: {
    textAlign: 'center',
    width: 314,
  }
});

const Register = (props: any) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmedPassword, setConfirmedPassword] = useState('');

  useEffect(() => {
    return props.navigation.addListener('state', () => {
      setUsername('');
      setEmail('');
      setPassword('');
      setConfirmedPassword('');
      props.cleanFormErrors('register', null);
    });
  }, [props.navigation]);

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <TextInputField
          name="username"
          label={props.messages['register.form.username']}
          error={props.error ? props.error : null}
          form='register'
          value={username}
          setInputValue={setUsername}/>

        <TextInputField
          name="email"
          label={props.messages['register.form.email']}
          error={props.error ? props.error : null}
          form='register'
          value={email}
          setInputValue={setEmail}/>

        <TextInputField
          name="password"
          label={props.messages['register.form.password']}
          error={props.error ? props.error : null}
          form='register'
          value={password}
          setInputValue={setPassword}/>

        <TextInputField
          name="confirmedPassword"
          label={props.messages['register.form.confirmed_password']}
          error={props.error ? props.error : null}
          form='register'
          value={confirmedPassword}
          setInputValue={setConfirmedPassword}/>

        <TouchableOpacity style={styles.button} onPress={() => {
          props.fetchData(props.apiHost + '/register', JSON.stringify({
            username: username,
            email: email,
            password: password,
            confirmedPassword: confirmedPassword
          }), 'register', 'POST', props.navigation);
        }}>
          <Text style={styles.buttonText}>{props.messages['register.form.btn'].toUpperCase()}</Text>
        </TouchableOpacity>

        <Text
          style={styles.link}
          onPress={() => props.navigation.navigate('login')}>
            {props.messages['register.link.login']}
        </Text>
      </ScrollView>
    </SafeAreaView>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
