export const initialState = {
  user: {
    loginResponse: {
      data: null,
      isPending: false,
      error: null
    },
    registerResponse: {
      data: null,
      isPending: false,
      error: null
    }
  }
};

export const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case 'FETCH_LOGIN_PENDING':
      return {
        ...state,
        loginResponse: {
          isPending: true
        }
      };
    case 'FETCH_REGISTER_PENDING':
      return {
        ...state,
        registerResponse: {
          isPending: true
        }
      };
    case 'FETCH_LOGIN_SUCCESS':
      return {
        ...state,
        loginResponse: {
          isPending: false,
          data: action.response
        }
      };
    case 'FETCH_REGISTER_SUCCESS':
      return {
        ...state,
        registerResponse: {
          isPending: false,
          data: action.response
        }
      };
    case 'FETCH_LOGIN_ERROR':
      return {
        ...state,
        loginResponse: {
          isPending: false,
          error: action.error
        }
      };
    case 'FETCH_REGISTER_ERROR':
      return {
        ...state,
        registerResponse: {
          isPending: false,
          error: action.error
        }
      };
    case 'CLEAN_FORM_ERRORS':
      switch (action.formName) {
        case 'login':
          if (!action.fieldName) {
            state.loginResponse.error = null;

            return {
              ...state
            };
          } else {
            switch (action.fieldName) {
              case 'email':
                state.loginResponse.error && state.loginResponse.error.email ? state.loginResponse.error.email = null : null;

                return {
                  ...state
                };
              case 'password':
                state.loginResponse.error && state.loginResponse.error.password ? state.loginResponse.error.password = null : null;

                return {
                  ...state
                };
              default:
                return state;
            }
          }
        case 'register':
          if (!action.fieldName) {
            state.registerResponse.error = null;

            return {
              ...state
            };
          } else {
            switch (action.fieldName) {
              case 'username':
                state.registerResponse.error && state.registerResponse.error.username ? state.registerResponse.error.username = null : null;

                return {
                  ...state
                };
              case 'email':
                state.registerResponse.error && state.registerResponse.error.email ? state.registerResponse.error.email = null : null;

                return {
                  ...state
                };
              case 'password':
                state.registerResponse.error && state.registerResponse.error.password ? state.registerResponse.error.password = null : null;

                return {
                  ...state
                };
              case 'confirmedPassword':
                state.registerResponse.error && state.registerResponse.error.confirmedPassword ? state.registerResponse.error.confirmedPassword = null : null;

                return {
                  ...state
                };
              default:
                return state;
            }
          }
        default:
          return state;
      }
    default:
      return state;
  }
};
