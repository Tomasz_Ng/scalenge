import routes from '../config/routes';
import * as Localization from 'expo-localization';
import messages from '../translations';

const locale = Localization.locale.substr(0,2);

export const initialState = {
  root: {
    routes: routes,
    route: 'home',
    locale: locale,
    messages: !messages[locale] ? messages['en'] : messages[locale],
    apiHost: process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'https://scalenge-api.herokuapp.com'
  }
};

export const rootReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case '@@INIT':
      return state;
    case 'df':
      return state;
    default:
      return state;
  }
};
