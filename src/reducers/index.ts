import {combineReducers} from 'redux';
import {rootReducer, initialState as rootState} from './rootReducer';
import {userReducer, initialState as userState} from './userReducer';

export const initialState = Object.assign(rootState, userState);

export const reducers = () => combineReducers({
    'root': rootReducer,
    'user': userReducer
});
